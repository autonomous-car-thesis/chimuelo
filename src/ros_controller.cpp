
#include <controller_interface/controller.h>
#include <hardware_interface/joint_command_interface.h>
#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>
#include <sstream>
#include <chimuelo_hardware_interface.h>
#include <chimuelo_hardware.h>
#include <joint_limits_interface/joint_limits_interface.h>
#include <joint_limits_interface/joint_limits.h>
#include <joint_limits_interface/joint_limits_urdf.h>
#include <joint_limits_interface/joint_limits_rosparam.h>
//#include <tr1cpp/joint.h>

using namespace hardware_interface;
using joint_limits_interface::JointLimits;
using joint_limits_interface::SoftJointLimits;
using joint_limits_interface::PositionJointSoftLimitsHandle;
using joint_limits_interface::PositionJointSoftLimitsInterface;

namespace chimuelo_hardware_interface {
//try to update using the ackermann controller 
	ChimueloHardwareInterface::ChimueloHardwareInterface(ros::NodeHandle& nh) : nh_(nh){
		init();
		controller_manager_.reset(new controller_manager::ControllerManager(this, nh_));
        nh_.param("/chimuelo/hardware_interface/loop_hz", loop_hz_, 0.1);
        ros::Duration update_freq = ros::Duration(1.0/loop_hz_);
        non_realtime_loop_ = nh_.createTimer(update_freq, &ChimueloHardwareInterface::update, this);

	}
	//Dont fully understand this ~ChimueloHardwareInterface::ChimueloHardwareInterface
	ChimueloHardwareInterface::~ChimueloHardwareInterface() {

	}

	void ChimueloHardwareInterface::init(){

		//Get joint names
		nh_.getParam("/chimuelo/hardware_interface/joints",joint_names_);
		num_joints_ = joint_names_.size();

		//Resize vectors
		joint_position_.resize(num_joints_);
		joint_velocity_.resize(num_joints_);
        joint_effort_.resize(num_joints_);
        joint_position_command_.resize(num_joints_);
        joint_velocity_command_.resize(num_joints_);
        joint_effort_command_.resize(num_joints_);

        //Initialize Controller 
        for (int i = 0; i < num_joints_; i++){

        	//Ask for this 
        	//https://github.com/SlateRobotics/tr1cpp/blob/master/include/tr1cpp/joint.h
        	
        	//get the joint names stored in joint_names_ with structure std::vector<std::string> 
        	//Register each joint as shown here https://github.com/ros-controls/ros_control/wiki/hardware_interface
        	//for std::vector<double>joint_position_; std::vector<double> joint_velocity_; std::vector<double> joint_effort_;
        
        	JointStateHandle jointStateHandle(joint_names_[i], &joint_position_[i], &joint_velocity_[i], &joint_effort_[i]);
        	joint_state_interface_.registerHandle(jointStateHandle);

        	//Create position joint interface 
        	JointHandle jointPositionHandle(jointStateHandle, &joint_position_command_[i]);
            JointLimits limits;
            SoftJointLimits softLimits;

            if(getJointLimits(joint_names_[i], nh_, limits)==false){
            	ROS_ERROR_STREAM("Cannot set joint limits for " << joint_names_[i]);
            }
            else{
            	PositionJointSoftLimitsHandle jointLimitsHandle(jointPositionHandle, limits, softLimits);
            	positionJointSoftLimitsInterface.registerHandle(jointLimitsHandle);
            }
           	
           	//Should we register the limits for the effort interfaces?
            position_joint_interface_.registerHandle(jointPositionHandle);

            // Create effort joint interface
			JointHandle jointEffortHandle(jointStateHandle, &joint_effort_command_[i]);
			effort_joint_interface_.registerHandle(jointEffortHandle);

        }

        registerInterface(&joint_state_interface_);
        registerInterface(&position_joint_interface_);
        registerInterface(&effort_joint_interface_);
        registerInterface(&positionJointSoftLimitsInterface);



	}
	void ChimueloHardwareInterface::starting(const ros::Time& time){
	        
	        for (int i = 0; i < num_joints_; i++){
	        	joint_= hw->getHandle(joint_names_[i]);
	        	joint_init_position_[i]=(joint_).getPosition();
        		
        }
	}
	void ChimueloHardwareInterface::update(const ros::TimerEvent& e){

		_logInfo = "\n";
		_logInfo += "Joint Position Command:\n";
		for (int i = 0; i < num_joints_; i++)
		{
			std::ostringstream jointPositionStr;
			jointPositionStr << joint_position_command_[i];
			_logInfo += "  " + joint_names_[i] + ": " + jointPositionStr.str() + "\n";
		}

		elapsed_time_ = ros::Duration(e.current_real - e.last_real);
        read();
        controller_manager_->update(ros::Time::now(), elapsed_time_);
        write(elapsed_time_);

	}

	void ChimueloHardwareInterface::read(){
		//getJoint() is a public method? It is actually from effort_controllers
		_logInfo = "\n";
		for (int i = 0; i < num_joints_; i++){
			std::ostringstream jointPositionStr;
			joint_= hw->getHandle(joint_names_[i]);
			joint_position_[i] = (joint_).getPosition();
			jointPositionStr << joint_position_[i];
			_logInfo += "Joint:" + joint_names_[i] + "current position: " + jointPositionStr.str() + "\n";

		}
	}

	void ChimueloHardwareInterface::write(ros::Duration elapsed_time){

		positionJointSoftLimitsInterface.enforceLimits(elapsed_time);
		for (int i = 0; i < num_joints_; i++){

			double desired_pos = joint_init_position_[i] + sin(ros::Time::now().toSec());
			double current_pos = joint_position_[i]; 

			joint_= hw->getHandle("joint_front_left");
			//(joint_names_[i]); //joint_front_left

			joint_effort_command_[i] = -10 * (current_pos - desired_pos); 
			double effor =  joint_effort_command_[i]; 
			joint_.setCommand(effor);

		}
		
	}
}//Class

/*PLUGINLIB_EXPORT_CLASS(
	chimuelo_controller_ns::PositionController,  //locate
	controller_interface::ControllerBase);//ns*/