#include "chimuelo_hardware_interface.h"
#include <ros/ros.h>

int main(int argc, char *argv[]){

	ros::init(argc, argv, "chimuelo_hardware_interface");
	ROS_INFO("Chimuelo Hardware Interface online");
	ros::NodeHandle nh;
	ros::AsyncSpinner spinner(1);
    spinner.start();
	chimuelo_hardware_interface::ChimueloHardwareInterface chimuelo(nh);
	ros::waitForShutdown();
	return 0;
}