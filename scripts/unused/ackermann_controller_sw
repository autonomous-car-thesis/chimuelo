#!/usr/bin/env python
import numpy as np
import math
import rospy 
import threading
from std_msgs.msg import Float64
from std_msgs.msg import String 
from geometry_msgs.msg import Twist
from ackermann_msgs.msg import AckermannDriveStamped 
from ackermann_msgs.msg import AckermannDrive

from controller_manager_msgs.srv import ListControllers




class _AckermannCtrlr(object):


  """Ackermann controller

  An object of class _AckermannCtrlr is a node that controls the wheels of a
  vehicle with Ackermann steering.
  """


  def __init__(self):

    rospy.init_node("ackermann_controller", anonymous=True)
    self._DEF_CMD_TIMEOUT = 0.5  # Default command timeout. Unit: second.
    self._DEF_PUB_FREQ = 30.0    # Default publishing frequency. Unit: hertz

    # Parameters
    # Wheels controler's name for each one also for steering and rolling
    (left_steer_link_name, left_steer_ctrlr_name,left_front_axle_ctrlr_name) = self.getfrontwheelparams("left")

    (right_steer_link_name, right_steer_ctrlr_name,right_front_axle_ctrlr_name) = self.getfrontwheelparams("right")

    (left_rear_link_name, left_rear_axle_ctrlr_name) = self.getRearWheelParams("left")


    (_right_rear_link_name, right_rear_axle_ctrlr_name) = self.getRearWheelParams("right")

    #Call for all the controllers available 
    list_ctrlrs = rospy.ServiceProxy("controller_manager/list_controllers",
                                         ListControllers)
    list_ctrlrs.wait_for_service()
    # _last_cmd_time is the time at which the most recent Ackermann
    # driving command was received.
    self.sleep_rate= rospy.Rate(self._DEF_PUB_FREQ)

    self._last_cmd_time = rospy.get_time()  #Maybe should be remove

    self._steer_ang_img = 0.5    #Set point from image topic
        #Geometry dimensions in meters measured at the model in blender Units: meters
    self._width = 0.4635
    self._length = 0.7386
    self.radius = 0.15/2
    #Distance btw CG and the wheels
    self.distanceW2CG = np.array([ math.sqrt(self._width**2+self._length**2) ,math.sqrt(self._width**2+self._length**2),(self._width/2),(self._width/2)])

    #Variables for compute velocity wheels base on teorical steering angle
    self._theta_steer = np.array([0.0,0.0]) # Vector of steering joint angle [0][0] used for teorical [1][1] used for setpoint Steer_img
    self.wheel_ang_vel = np.array([0.0,0.0,0.0,0.0]) # Vector of angular velocities for each wheel in local frame  
    self._Phi_global = np.array([[0.0],[0.0],[0.0]])# Vector of velocity in global frame 
    self.steer_ang = 0.0      # Steering angle
    self._steer_ang_vel = 0.0  # Steering angle velocity
    self._speed = 0.0
    self._accel = 0.0          # Acceleration
    #self._frame_id; 
    
    # Publishers and subscribers
    """ We subcribe to twits cmd_vel to obtain linear X and angular Z velocities
        we publish directly to the controller for each Steer/roll
        Also we pulish the vaulues to the ackerman_cmd in"""
    self._left_steer_cmd_pub = self._create_cmd_pub(list_ctrlrs, left_steer_ctrlr_name)
    self._right_steer_cmd_pub = self._create_cmd_pub(list_ctrlrs, right_steer_ctrlr_name)
    self._left_front_axle_cmd_pub = self._create_axle_cmd_pub(list_ctrlrs, left_front_axle_ctrlr_name)
    self._right_front_axle_cmd_pub = \
    self._create_axle_cmd_pub(list_ctrlrs, right_front_axle_ctrlr_name)
    self._left_rear_axle_cmd_pub = \
    self._create_axle_cmd_pub(list_ctrlrs, left_rear_axle_ctrlr_name)
    self._right_rear_axle_cmd_pub = \
    self._create_axle_cmd_pub(list_ctrlrs, right_rear_axle_ctrlr_name)
    
    ackermann_cmd_topic = rospy.get_param('~ackermann_cmd_topic', '/ackermann_cmd')
    
    self.ackermann_cmd_pub = rospy.Publisher(ackermann_cmd_topic,AckermannDriveStamped,queue_size=1)
    twist_cmd_topic = rospy.get_param('~twist_cmd_topic', '/cmd_vel')
    self._twist_cmd_sub = rospy.Subscriber(twist_cmd_topic, Twist,self.cmd_vel_callback, queue_size=1)
    #self.image_steering_topic = rospy.Subscriber("/steer_angle_img",Float64,self.callback_str_img)

    #self.ackermann_pack_pub = rospy.Publisher('custom_ackerMsg',String,queue_size=1)


    #self._frame_id = rospy.get_param('~frame_id', 'odom')
  def _create_cmd_pub(self, list_ctrlrs, ctrl_name):

      #Create a command publisher
    return rospy.Publisher(ctrl_name + '/command', Float64,queue_size = 1)

  def _create_axle_cmd_pub(self, list_ctrlrs, axle_ctrlr_name): 

      #Create an axle command publisher.
    if not axle_ctrlr_name:
        return None
    return self._create_cmd_pub(list_ctrlrs, axle_ctrlr_name)

  def cmd_vel_callback(self, data):

      """Twist cmd_vel command callback

      :Parameters:
      cmd_vel : geometry_msgs/Twist Message
      vector3 linear (float64)(x,y,z)
      vector3 angular (float64)(x,y,z)
      """ 
      self._Phi_global[0][0] = data.linear.x 
      self._Phi_global[1][0] = 0
      self._Phi_global[2][0] = data.angular.z
      #self._Phi_global[0][0] = 0.5 
      #self._Phi_global[1][0] = 0
      #self._Phi_global[2][0] = 0

  def callback_str_img(self,angle):
      #print(type(data))
      self._steer_ang_img = angle.data #Angle to Rotate the global to local 


  def getfrontwheelparams(self, side):
    # Get front wheel parameters. Return a tuple containing the steering
    # link name, steering controller name, axle controller name (or None),
    # and inverse of the circumference.
    prefix = "~" + side + "_front_wheel/"
    
    steer_link_name  = rospy.get_param(prefix + "steering_link_name",
                                       "link_front_"+side)
    steer_ctrlr_name = rospy.get_param(prefix + "steering_controller_name",
                                       side + "_steering_ctrlr")
    axle_ctrlr_name = rospy.get_param(prefix + "axle_controller_name",
                                        side + "_front_axle_ctrlr")
    print "Theta steer_link_name:",steer_link_name
    print "Theta steer_ctrlr_name:",steer_ctrlr_name
    print "Theta axle_ctrlr_name:",axle_ctrlr_name



    return steer_link_name, steer_ctrlr_name, axle_ctrlr_name

  def getRearWheelParams(self, side):
    # Get rear wheel parameters. Return a tuple containing the link name,
    # axle controller name, and inverse of the circumference.
    prefix = "~" + side + "_rear_wheel/"
    if(side =="left"):

      link_name = rospy.get_param(prefix + "link_name", "link_wheel_bl")
      axle_ctrlr_name = rospy.get_param(prefix + "axle_controller_name",
                                        side + "_rear_axle_ctrlr")
      print "Theta link_name:",link_name
      print "Theta axle_ctrlr_name:",axle_ctrlr_name
      
    
    else:


      link_name = rospy.get_param(prefix + "link_name", "link_wheel_br")
      axle_ctrlr_name = rospy.get_param(prefix + "axle_controller_name",
                                        side + "_rear_axle_ctrlr")
      print "Theta link_name:",link_name
      print "Theta axle_ctrlr_name:",axle_ctrlr_name
    

    return link_name, axle_ctrlr_name;
    
  #Now adding rolling constring and slading constraing to the kinematics motion
  # Methode for for control the vehicle using Sliding constraint & Rolling constraint 
  def spin (self):


    while not rospy.is_shutdown():

        #First value of ICR distance for theorical Vlinear/Vangular
        ICR_distance = np.array([0.0])
        Rotational = np.array([[np.cos(self._steer_ang_img), np.sin(self._steer_ang_img), 0.0],
          [-1*np.sin(self._steer_ang_img), np.cos(self._steer_ang_img), 0.0],
          [0.0,0.0,1.0]])
        
        #self._Phi_global[0][0] = 0.5
        #self._Phi_global[1][0] = 0.0
        #self._Phi_global[2][0] = 0.0

        self._Phi_global= np.dot(Rotational,self._Phi_global)
        print "Phi rotated:",self._Phi_global
        #Motion for turn and move
        if(self._Phi_global[0][0]!=0 and self._Phi_global[2][0]!=0):  
          ICR_distance[0] = (self._Phi_global[0][0]/self._Phi_global[2][0]) #Radio of ICR
          self.steer_ang = math.tan(self._length/ICR_distance[0]) #Angle of virtual wheel in center of the vehicle
          print "++++++++++++++++++go straight and turn+++++++++++++++++"
        
        #If the value of Z dot is zero go straight
        if(self._Phi_global[2][0]==0):
          ICR_distance[0] = 0;
          self.steer_ang = 0; #Virtual wheel in center of the vehicle
          print "++++++++++++++++++Zero turning+++++++++++++++++"
        
        #For online motion to left, means without linear velocity 
        if (self._Phi_global[0][0] == 0 and self._Phi_global[2][0] > 0):
          ICR_distance[0] = 0.5;
          self.steer_ang = math.tan(self._length/ICR_distance[0]) #Virtual wheel in center of the vehicle
          print "++++++++++++++++++turn left+++++++++++++++++"
        #For online motion to right, means without linear velocity 
        if (self._Phi_global[0][0] == 0 and self._Phi_global[2][0] < 0):
          ICR_distance[0] = -0.5;
          #Angle in radians of the steering virtual front  wheel
          self.steer_ang = math.tan(self._length/ICR_distance[0]) #Virtual wheel in center of the vehicle
          print "++++++++++++++++++turn right+++++++++++++++++"
        
        

        #Angle btw L's and Xg+ 
        # Count the wheels counter clockwise a - b - c - d
        alphaA = math.atan2(-self._width/2,self._length)
        alphaB = math.atan2(self._width/2,self._length)
        alphaC = math.pi/2 
        alphaD = -1*math.pi/2
  
        #Turn to left or right for theorical steer angle
        if(self.steer_ang > 0):
          #Angles reference to ICR for turn left
          SigmaAt = math.atan2(self._length,math.fabs(ICR_distance[0]+(self._width/2)))
          SigmaBt = math.atan2(self._length,math.fabs(ICR_distance[0]-(self._width/2)))
          #Angle btw wheel's axe and Global Y+   
          betaC = 0
          betaD = math.pi 
          self._theta_steer[1] = math.pi/2 - SigmaBt + alphaB 
          self._theta_steer[0] = math.pi/2 - SigmaAt + alphaA
          print "#Turn to left"
          #Turn to left
        elif(self.steer_ang < 0):
          #Angles reference to ICR for turn right
          SigmaAt = math.atan2(self._length,math.fabs(ICR_distance[0]-(self._width/2)))
          SigmaBt = math.atan2(self._length,math.fabs(ICR_distance[0]+(self._width/2)))
          #Angle btw wheel's axe and Global Y+   
          betaC = 0
          betaD = math.pi 
          self._theta_steer[1] = math.pi/2 - SigmaBt + alphaB 
          self._theta_steer[0] = math.pi/2 - SigmaAt + alphaA
          print "#Turn to right"
        else:
          #Angles reference to ICR for turn left 
          SigmaAt = 0
          SigmaBt = 0
          #Angle btw wheel's axe and Global Y+  
          betaC = 0
          betaD = math.pi 
          betaB = math.pi/2 - SigmaBt + alphaB 
          betaA = math.pi/2 - SigmaAt + alphaA
          self._theta_steer[1] = math.pi/2 - SigmaBt + alphaB 
          self._theta_steer[0] = math.pi/2 - SigmaAt + alphaA
          print "#go straight"
          
        
        #Rolling constraint for theorical values
        J12 =  np.array([[math.sin(alphaA+self._theta_steer[0]),-1*math.cos(alphaA+self._theta_steer[0]),-1*self.distanceW2CG[0]*math.cos(self._theta_steer[0])],
        [math.sin(alphaB+self._theta_steer[1]),-1*math.cos(alphaB+self._theta_steer[1]),-1*self.distanceW2CG[1]*math.cos(self._theta_steer[1])],
        [math.sin(alphaC+betaC),-1*math.cos(alphaC+betaC),-1*self.distanceW2CG[2]*math.cos(betaC)],
        [math.sin(alphaD+betaD),-1*math.cos(alphaD+betaD),-1*self.distanceW2CG[3]*math.cos(betaD)]])
        #Sliding constraint
        C12 =  np.array([[math.cos(alphaA+self._theta_steer[0]),math.sin(alphaA+self._theta_steer[0]),self.distanceW2CG[0]*math.sin(self._theta_steer[0])],
        [math.cos(alphaB+self._theta_steer[1]),math.sin(alphaB+self._theta_steer[1]),self.distanceW2CG[1]*math.sin(self._theta_steer[1])],
        [math.cos(alphaC+betaC),math.sin(alphaC+betaC),self.distanceW2CG[2]*math.sin(betaC)],
        [math.cos(alphaD+betaD),math.sin(alphaD+betaD),self.distanceW2CG[3]*math.sin(betaD)]])


        J2 = np.eye(4,4)*self.radius # Both back wheels gives same constrains?
        
        A = np.concatenate([J12,C12],axis=0)
        #print "A matrix:", A
        zeros = np.zeros((4,4))
        B = np.concatenate([J2,zeros],axis=0)
        #print "Matrix B:", B
        B_trmp = np.transpose(B)
        #print "B transpose:", B_trmp
        B_inv = np.dot(B_trmp,B)
        #print "B inv 1:", B_inv
        B_inv = np.linalg.inv(B_inv)
        #print "B inv 2:", B_inv
        A_trmp = np.dot(B_trmp,A)
        #print "A dot:", A_trmp

        B_net = np.dot(B_inv,A_trmp)
        #print "B net:", B_net

        #print "Phi_global:", self._Phi_global

        # Translation speed of each wheel in local frame Consider back wheels moving as same speed
        Result = np.dot(B_net,self._Phi_global)
        #print "Result :", Result.shape
        print "Result matrix:",Result
        self.wheel_ang_vel[0] = Result[0] #Wheel vel for A 
        self.wheel_ang_vel[1] = Result[1] #Wheel vel for B 
        self.wheel_ang_vel[2] = Result[2] #Wheel vel for C
        self.wheel_ang_vel[3] = Result[2] #Wheel vel for D

        

        print "Theta steer:",self._theta_steer
        
        print "Angular Velocity:", self.wheel_ang_vel
        print "-----------------"
        #Publish the steering and rolling joint commands.
        #Steering for controller
        self._left_steer_cmd_pub.publish(self._theta_steer[1])
        self._right_steer_cmd_pub.publish(self._theta_steer[0]) 
        
        #Rolling for controller
        self._left_front_axle_cmd_pub.publish(self.wheel_ang_vel[1])
        self._right_front_axle_cmd_pub.publish(self.wheel_ang_vel[0])
        self._left_rear_axle_cmd_pub.publish(self.wheel_ang_vel[2])
        self._right_rear_axle_cmd_pub.publish(self.wheel_ang_vel[3])



        #Upload the values to ackermann_cmd
        msg = AckermannDriveStamped()
        msg.header.stamp = rospy.Time.now()
        #msg.header.frame_id = self._frame_id
        msg.drive.steering_angle = self._steer_ang_img
        msg.drive.speed = self._Phi_global[0][0]
        msg.drive.steering_angle_velocity = self._Phi_global[2][0]

        self.ackermann_cmd_pub.publish(msg)

        """Upload the values to the ackermann_pack_node 
        This alow us to see all the values relat to the motion in the ackermann
        like: angular vel of wheels - Result; linear vel Phi_global[0][0]
         """

        #Sleep the node for the frecuency until next value
        self.sleep_rate.sleep()

def main(argv):
    # we eventually get the ns (namespace) from the ROS parameter server for this node
    ns=''
    node = cmdvel2gazebo(ns)
    rate = rospy.Rate(100) # run at 10Hz
    while not rospy.is_shutdown():
        node.publish()
        rate.sleep()
if __name__ == '__main__':
  ack_motion = _AckermannCtrlr()
  rospy.loginfo("Ackermann controller On ")
  ack_motion.spin()


