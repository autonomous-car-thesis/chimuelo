#!/usr/bin/env python
import numpy as np
import math
import rospy 
from math import pi
from std_msgs.msg import Float64
from geometry_msgs.msg import Twist
from ackermann_msgs.msg import AckermannDriveStamped 
from ackermann_msgs.msg import AckermannDrive

from controller_manager_msgs.srv import ListControllers


class Ackermann_kinematic_control(object):

	def __init__(self):
			## Parameters
			# Wheels
			(left_steer_link_name, left_steer_ctrlr_name,
		         left_front_axle_ctrlr_name, self._left_front_inv_circ) = \
		                         self._get_front_wheel_params("left")
			(right_steer_link_name, right_steer_ctrlr_name,
		         right_front_axle_ctrlr_name, self._right_front_inv_circ) = \
		                         self._get_front_wheel_params("right")
			(left_rear_link_name, left_rear_axle_ctrlr_name,
		         self._left_rear_inv_circ) = \
		                         self._get_rear_wheel_params("left")
			(self._right_rear_link_name, right_rear_axle_ctrlr_name,
		         self._right_rear_inv_circ) = \
		                         self._get_rear_wheel_params("right")
			list_ctrlrs = rospy.ServiceProxy("controller_manager/list_controllers",
                                     ListControllers)
    	list_ctrlrs.wait_for_service()
    	self._ackermann_cmd_lock = threading.Lock()
    	self._steer_ang = 0.0      # Steering angle
    	self._steer_ang_vel = 0.0  # Steering angle velocity
    	self._speed = 0.0
    	self._accel = 0.0          # Acceleration
    	self._last_steer_ang = 0.0  # Last steering angle
    	self._theta_left = 0.0      # Left steering joint angle
    	self._theta_right = 0.0     # Right steering joint angle

    	self._last_speed = 0.0
    	self._last_accel_limit = 0.0  # Last acceleration limit
    	# Axle angular velocities
    	self._left_front_ang_vel = 0.0
    	self._right_front_ang_vel = 0.0
    	self._left_rear_ang_vel = 0.0
    	self._right_rear_ang_vel = 0.0
			#Geometry dimensions in meters measured at the model in blender	Units: meters
			self._width = 0.4635;
  		self._length = 0.7386; 
			self.radius = 0.15/2; 
			
			#Distance btw CG and the wheels
			self.la = math.sqrt(width**2+length**2)
			self.lb = la
			self.lc = width/2
			self.ld = lc 

  		# Publishers and subscribers

    	self._left_steer_cmd_pub = \
            _create_cmd_pub(list_ctrlrs, left_steer_ctrlr_name)
    	self._right_steer_cmd_pub = \
            _create_cmd_pub(list_ctrlrs, right_steer_ctrlr_name)

    	self._left_front_axle_cmd_pub = \
            _create_axle_cmd_pub(list_ctrlrs, left_front_axle_ctrlr_name)
    	self._right_front_axle_cmd_pub = \
            _create_axle_cmd_pub(list_ctrlrs, right_front_axle_ctrlr_name)
    	self._left_rear_axle_cmd_pub = \
            _create_axle_cmd_pub(list_ctrlrs, left_rear_axle_ctrlr_name)
    	self._right_rear_axle_cmd_pub = \
            _create_axle_cmd_pub(list_ctrlrs, right_rear_axle_ctrlr_name)

    	self._ackermann_cmd_sub = \
        	  rospy.Subscriber("ackermann_cmd", AckermannDrive,
                             self.ackermann_cmd_cb, queue_size=1)

  def _create_cmd_pub(list_ctrlrs,ctrl_name):
    	#Create a command publisher.
    	return rospy.Publisher(ctrl_name + "/command", Float64, queue_size=1)

	def _create_axle_cmd_pub(list_ctrlrs,axle_ctrlr_name):
			# Create an axle command publisher.
    	if not axle_ctrlr_name:
        return None
    	return _create_cmd_pub(list_ctrlrs, axle_ctrlr_name)

  def _ackermann_cmd_cb(self, ackermann_cmd):
        """Ackermann driving command callback

        :Parameters:
          ackermann_cmd : ackermann_msgs.msg.AckermannDrive
            Ackermann driving command.
        """
        self._last_cmd_time = rospy.get_time()
        with self._ackermann_cmd_lock:
            self._steer_ang = ackermann_cmd.steering_angle
            self._steer_ang_vel = ackermann_cmd.steering_angle_velocity
            self._speed = ackermann_cmd.speed
            self._accel = ackermann_cmd.acceleration

  def _get_front_wheel_params(self, side):
        # Get front wheel parameters. Return a tuple containing the steering
        # link name, steering controller name, axle controller name (or None),
        # and inverse of the circumference.

        prefix = "~" + side + "_front_wheel/"
        steer_link_name  = rospy.get_param(prefix + "steering_link_name",
                                           "link_front_"+side+"_dir")
        steer_ctrlr_name = rospy.get_param(prefix + "steering_controller_name",
                                           side + "_steering_ctrlr")
        axle_ctrlr_name  = rospy.get_param(prefix + "axle_controller_name",
                                          None)
        dia = float(rospy.get_param(prefix + "diameter",
                                        self._DEF_WHEEL_DIA))
        inv_circ = 1/(pi*dia)

        return steer_link_name, steer_ctrlr_name, axle_ctrlr_name, inv_circ

  def _get_rear_wheel_params(self, side):
        # Get rear wheel parameters. Return a tuple containing the link name,
        # axle controller name, and inverse of the circumference.

        prefix = "~" + side + "_rear_wheel/"
        link_name = rospy.get_param(prefix + "link_name", "link_back_"+side)
        axle_ctrlr_name  = rospy.get_param(prefix + "axle_controller_name",
                                          None), 
        dia = float(rospy.get_param(prefix + "diameter",
                                        self._DEF_WHEEL_DIA))
        inv_circ = 1/(pi*dia)
        return link_name, axle_ctrlr_name, inv_circ
		#Now adding rolling constring and slading constraing to the kinematics motion

		# Sliding constraint & Rolling constraint control the vehicle
	def spin (self): 

			last_time = rospy.get_time()

			while not rospy.is_shutdown():
			t = rospy.get_time()
      delta_t = t - last_time
			with self._ackermann_cmd_lock:
                    steer_ang = self._steer_ang
                    steer_ang_vel = self._steer_ang_vel
                    speed = self._speed
                    accel = self._accel
		
		steer_ang_changed, center_y 
		theta=0; 
		ydot = 0;

		# Compute theta, the virtual front wheel's desired steering angle.
      if steer_ang_vel > 0.0:
            # Limit the steering velocity.
            ang_vel = (steer_ang - self._last_steer_ang) / delta_t
            ang_vel = max(-steer_ang_vel,
                           min(ang_vel, steer_ang_vel))
            theta = self._last_steer_ang + ang_vel * delta_t
      else:
            theta = steer_ang #Must be in radians 
    # Compute the desired steering angles for the left and right front
    # wheels.


		#Rotational matrix 
		Rotation = 	np.array([[math.cos(theta),math.sin(theta),0],
				[-math.sin(theta),math.cos(theta),0],[0, 0, 1]])
	
		#Velocity vector in global frame 
		phiG = np.array([[speed],[ydot],[steer_ang_vel]])

		#Velocity vector in local frame
		phiL = np.dot(Rotation,phiG);

		#Angle btw L's and Xg+ 
		# Count the wheels counter clockwise a - b - c - d
		alphaA = math.atan2(-self.width/2,self.length)
		alphaB = math.atan2(self.width/2,self.length)
		alphaC = pi/2 
		alphaD = -pi/2 

		steer_ang_changed = theta != self._last_steer_ang
		if steer_ang_changed: 
			self._last_steer_ang = theta

		#compute Velocity wheels for turn right or left 
        #turn left 90° 	&	140°
		if (theta > 90) and (theta < 140):

			#Angles reference to ICR for turn left 
			#ICR_distance = length / math.tan(thetaDot) #should i use arctan?
			ICR_distance = self.length / math.tan(theta) #coould this value be negative? 
			SigmaA = math.atan2(self.length,ICR_distance+(self.width/2))
			SigmaB = math.atan2(self.length,ICR_distance-(self.width/2))
			
			#Angle btw wheel's axe and CG  
			betaC = 0
			betaD = pi 
			betaB = pi/2 - SigmaB + alphaB 
			self._theta_left = betaB
			betaA = pi/2 - SigmaA + alphaA
			self._theta_right = betaA
			#print('turn left')
		#turn right 90° &&	30°
		elif (thetaDot < 90) and (thetaDot > 30):
			#Angles reference to ICR for turn right 
			ICR_distance = self.length / math.tan(theta)
			SigmaA = math.atan2(self.length,ICR_distance-(self.width/2))
			SigmaB = math.atan2(self.length,ICR_distance+(self.width/2))
			#Angle btw wheel's axe and CG  		
			betaC = 0
			betaD = pi 
			betaB = pi/2 + SigmaB - alphaB 
			self._theta_left = betaB
			betaA = pi/2 + SigmaA - alphaA
			self._theta_right = betaA
			
			#print('turn right')
		else:
			#Angles reference to ICR for turn left 
			SigmaA = 0
			SigmaB = 0
			#Angle btw wheel's axe and CG  
			betaC = 0
			betaD = pi 
			betaB = pi/2 - SigmaB + alphaB 
			betaA = pi/2 - SigmaA + alphaA
			self._theta_left = betaB
			self._theta_right = betaA
			#print('go straight')

		#Rolling constraint 
		J1 = 	np.array([[math.sin(alphaA+betaA),-math.cos(alphaA+betaA),self.la*math.cos(betaA)],
			[math.sin(alphaB+betaB),-math.cos(alphaB+betaB),self.lb*math.cos(betaB)],
			[math.sin(alphaC+betaC),-math.cos(alphaC+betaC),self.lc*math.cos(betaC)],
			[math.sin(alphaD+betaD),-math.cos(alphaD+betaD),self.ld*math.cos(betaD)]])
		#Sliding constraint
		C1 = 	np.array([[math.cos(alphaA+betaA),math.sin(alphaA+betaA),self.la*math.sin(betaA)],
			[math.cos(alphaB+betaB),math.sin(alphaB+betaB),self.lb*math.sin(betaB)],
			[math.cos(alphaC+betaC),math.sin(alphaC+betaC),self.lc*math.sin(betaC)],
			[math.cos(alphaD+betaD),math.sin(alphaD+betaD),self.ld*math.sin(betaD)]])
		J2 = np.eye(4,3)*self.radius # Both back wheels gives same constrains?

		A = np.concatenate([J1,C1],axis=0)
		B = np.concatenate([J2,np.eye(4,3)],axis=0)

		B_trmp = np.transpose(B)
		B_inv = np.dot(B_trmp,B)
		B_inv = np.linalg.inv(B_inv)

		A_trmp = np.dot(B_trmp,A)

		# spinning speed of each wheel. Consider back wheels moving as same speed
		Result = np.dot(np.dot(B_inv,A_trmp),phiL)

		self._left_front_ang_vel = Result[2]
		self._right_front_ang_vel = Result[1]
		self._left_rear_ang_vel = Result[3]
		self._right_rear_ang_vel = Result[3]

	# Publish the steering and axle joint commands.
		self._left_steer_cmd_pub.publish(self._theta_left)
    self._right_steer_cmd_pub.publish(self._theta_right)	
    self._left_front_axle_cmd_pub.publish(self._left_front_ang_vel)
    self._right_front_axle_cmd_pub.publish(self._right_front_ang_vel)
    self._left_rear_axle_cmd_pub.publish(self._left_rear_ang_vel)
    self._right_rear_axle_cmd_pub.publish(self._right_rear_ang_vel)


		#print('*******result*****')
	_DEF_WHEEL_DIA = 1.0    # Default wheel diameter. Unit: meter.
  _DEF_EQ_POS = 0.0       # Default equilibrium position. Unit: meter.
  _DEF_CMD_TIMEOUT = 0.5  # Default command timeout. Unit: second.
  _DEF_PUB_FREQ = 30.0    # Default publishing frequency. Unit: hertz

def main(args):
  rospy.init_node('ackerman_motion_node', anonymous=True)
  rospy.loginfo("Ackermann controller On ")

  ack_motion = Ackermann_kinematic_control()

  try:
    ack_motion.spin()
  except rospy.ROSInterruptException:
  	pass

