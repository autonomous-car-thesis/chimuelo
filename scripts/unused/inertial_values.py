#!/usr/bin/env python
import numpy as np

xx = 0.0
yy = 0.0
zz = 0.0
#Wheel mass
mass = 0.76886
radius = 0.0792374
length=0.0605 
I = np.array([[(mass/12)*(3*radius**2+length**2),0,0],[0,(mass/12)*(3*radius**2+length**2),0],[0,0,(mass/2)*radius**2]])
print I 