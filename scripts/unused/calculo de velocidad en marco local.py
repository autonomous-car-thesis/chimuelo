#!/usr/bin/env python
import numpy as np
import math
import rospy 
from std_msgs.msg import Float64
from geometry_msgs.msg import Twist
from ackermann_msgs.msg import AckermannDriveStamped 
from ackermann_msgs.msg import AckermannDrive

from controller_manager_msgs.srv import ListControllers


class Ackermann_kinematic_control(object):

	def __init__(self):
		
    twist_cmd_topic = rospy.get_param('~twist_cmd_topic','/cmd_vel')
    ackermann_cmd_topic = rospy.get_param('~ackermann_cmd_topic','/ackermann_cmd')
    frame_id = rospy.get_param('~frame_id','odom')

    list_ctrlrs = rospy.ServiceProxy("controller_manager/list_controllers",
                                     ListControllers)
    list_ctrlrs.wait_for_service()

    self.acker_sub = rospy.Subscriber(twist_cmd_topic,Twist,self.callback)
  	self.acker_pub = rospy.Publisher(ackermann_cmd_topic,AckermannDriveStamped,queue_size = 1)

  def callback(self,data):

  	pi = math.pi 
  	#Rotational angle (This is really useful?)
  	theta = 0; 
  	#Velocity values
  	xdot; 
  	ydot; 
  	thetaDot; #Is this the real stearing angle ?

  	#Geometry dimensions in meters measured at the model in blender
  	global width; width = 0.4635;
  	global length; length = 0.7386; 
		global radius; radius = 0.15/2;
		global frame_id 
		
		global pub 

		xdot = data.linear.x 
		ydot = data.linear.y
		thetaDot = data.angular.z 

		wheels_vel = self.kinematic_model(xdot,ydot,thetaDot)

		msg = AckermannDriveStamped()
		msg.header.stamp = rospy.Time.now()
		msg.header.frame_id = frame_id
		msg.drive.steaing_angle_velocity = wheels_vel

		pub.publish(msg)

		#msg.drive.steering_angle = thetaDot
    #msg.drive.speed = xdot

		#Now adding rolling constring and slading constraing to the kinematics motion

		# Sliding constraint & Rolling constraint
	def kinematic_model (xdot, ydot, thetaDot): 

	# geometric of the vehicle 
	
	#Rotational matrix 
	Rotation = 	np.array([[math.cos(theta),math.sin(theta),0],
				[-math.sin(theta),math.cos(theta),0],[0, 0, 1]])
	
	#Velocity vector in global frame 
	phiG = np.array([[xdot],[ydot],[thetaDot]])

	#Velocity vector in local frame
	phiL = np.dot();

	#Distance btw CG and the wheels
	la = math.sqrt(width**2+length**2)
	lb=la
	lc = width/2 
	ld= lc

	#Angle btw L's and Xg+ 
	# Count the wheels counter clockwise a - b - c - d
	alphaA = math.atan2(-width/2,length)
	alphaB = math.atan2(width/2,length)
	alphaC = pi/2 
	alphaD = -pi/2 

	#Take a decition for turn right or left 

	#Take a decition for turn right or left 
        #turn left 90° 	&	140°
	if (thetaDot > 90) and (thetaDot < 140):
			#Angles reference to ICR for turn left 
			ICR_distance = length * math.tan(thetaDot) #should i use arctan?
			SigmaA = math.atan2(length,ICR_distance+(width/2))
			SigmaB = math.atan2(length,ICR_distance-(width/2))
			#Angle btw wheel's axe and CG  
			betaC = 0
			betaD = pi 
			betaB = pi/2 - SigmaB + alphaB 
			betaA = pi/2 - SigmaA + alphaA
			print('turn left')
	#turn right 90° &&	30°
	elif (thetaDot < 90) and (thetaDot > 30):
			#Angles reference to ICR for turn right 
			ICR_distance = length / math.tan(thetaDot)
			SigmaA = math.atan2(length,ICR_distance-(width/2))
			SigmaB = math.atan2(length,ICR_distance+(width/2))
			#Angle btw wheel's axe and CG  		
			betaC = 0
			betaD = pi 
			betaB = pi/2 + SigmaB - alphaB 
			betaA = pi/2 + SigmaA - alphaA
			print('turn right')
	else:
			#Angles reference to ICR for turn left 
			SigmaA = 0
			SigmaB = 0
			#Angle btw wheel's axe and CG  
			betaC = 0
			betaD = pi 
			betaB = pi/2 - SigmaB + alphaB 
			betaA = pi/2 - SigmaA + alphaA
			print('go straight')

	#Rolling constraint 
	J1 = 	np.array([[math.sin(alphaA+betaA),-math.cos(alphaA+betaA),la*math.cos(betaA)],
			[math.sin(alphaB+betaB),-math.cos(alphaB+betaB),lb*math.cos(betaB)],
			[math.sin(alphaC+betaC),-math.cos(alphaC+betaC),lc*math.cos(betaC)],
			[math.sin(alphaD+betaD),-math.cos(alphaD+betaD),ld*math.cos(betaD)]])
	#Sliding constraint
	C1 = 	np.array([[math.cos(alphaA+betaA),math.sin(alphaA+betaA),la*math.sin(betaA)],
			[math.cos(alphaB+betaB),math.sin(alphaB+betaB),lb*math.sin(betaB)],
			[math.cos(alphaC+betaC),math.sin(alphaC+betaC),lc*math.sin(betaC)],
			[math.cos(alphaD+betaD),math.sin(alphaD+betaD),ld*math.sin(betaD)]])
	J2 = np.eye(4,3)*radius

	A = np.concatenate([J1,C1],axis=0)
	B = np.concatenate([J2,np.eye(4,3)],axis=0)

	B_trmp = np.transpose(B)
	B_inv = np.dot(B_trmp,B)
	B_inv = np.linalg.inv(B_inv)

	A_trmp = np.dot(B_trmp,A)

	Result = np.dot(np.dot(B_inv,A_trmp),phiL)
	print('*******result*****')
	return Result

def main(args):
  rospy.init_node('ackerman_motion_node', anonymous=True)
  rospy.loginfo("Ackermann controller On ")

  ack_motion = Ackermann_kinematic_control()

  try:
    ack_motion.spin()
  except rospy.ROSInterruptException:
  	pass

