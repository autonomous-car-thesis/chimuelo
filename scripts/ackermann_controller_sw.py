#!/usr/bin/env python
import numpy as np
import math, sys, getopt
import rospy 
import threading
from std_msgs.msg import Float64
from std_msgs.msg import MultiArrayLayout
from std_msgs.msg import MultiArrayDimension
from std_msgs.msg import Float32MultiArray
from std_msgs.msg import String 
from geometry_msgs.msg import Twist
from controller_manager_msgs.srv import ListControllers

#Note first tested with cmd__vel then use steer_angle_img to compute Wz


class _AckermannCtrlr():



  """Ackermann controller

  An object of class _AckermannCtrlr is a node that controls the wheels of a
  vehicle with Ackermann steering.
  """


  def __init__(self,ns):

    self.ns = ns
    rospy.init_node("ackermann_controller", anonymous=True)
    self._DEF_CMD_TIMEOUT = 0.5  # Default command timeout. Unit: second.
    self._DEF_PUB_FREQ = 50.0    # Default publishing frequency. Unit: hertz

    #Call for all the controllers available 
    list_ctrlrs = rospy.ServiceProxy("controller_manager/list_controllers",
                                         ListControllers)
    list_ctrlrs.wait_for_service()
    # _last_cmd_time is the time at which the most recent Ackermann
    # driving command was received.
    self.sleep_rate= rospy.Rate(self._DEF_PUB_FREQ)

    self._last_cmd_time = rospy.get_time()  #Maybe should be remove

    self._steer_ang_img = 0.0    #Set point from image topic - Steering angle Phi for virtual wheel
        #Geometry dimensions in meters measured at the model in blender Units: meters  r_chimuelo=0.15/2 l_chimuelo = 0.7386 w_chimuelo = 0.4635
    self._width = 1.301
    self._length = 2.62
    self.radius = 0.356
    #Distance btw CG and the wheels
    self.distanceW2CG = np.array([ math.sqrt(self._width**2+self._length**2) ,math.sqrt(self._width**2+self._length**2),(self._width/2),(self._width/2)])

    #Variables for compute velocity wheels base on steering angle
    self.betha_steer = np.array([0.0,0.0]) # Vector of steering joint angle [0] used for theta A [1] used for theta B
    self.wheel_ang_vel = np.array([0.0,0.0,0.0,0.0]) # Vector of angular velocities for each wheel in local frame  
    self._Phi_global = np.array([[1.0],[0.0],[0.0]])# Vector of velocity in global frame 
    
    #TODO create a new message type to change speed and accel
    #Off set for Bheta angle A and B 
    self.offsetR = 1.327434840282
    self.offsetL = 1.814157813307
    self.ICR_distance = np.array([0.0])

    self.array_HwCommands = Float32MultiArray()
    self.array_HwCommands.layout.dim.append(MultiArrayDimension())
    self.array_HwCommands.layout.dim[0].label = "Hw_Commands" 
    self.array_HwCommands.layout.dim[0].size = 6
    self.array_HwCommands.layout.dim[0].stride = 1
    self.array_HwCommands.layout.data_offset = 0

    # TODO: set timeout from launch file or rosparam
    self.timeout=rospy.Duration.from_sec(0.2);
    self.lastMsg=rospy.Time.now()

    self.maxsteerInside=0.6;
    # tan(maxsteerInside) = wheelbase/radius --> solve for max radius at this angle
    rMax = self._length/math.tan(self.maxsteerInside);
    # radius of inside tire is rMax, so radius of the ideal middle tire (rIdeal) is rMax+treadwidth/2
    rIdeal = rMax+(self._width/2.0)
    # tan(angle) = wheelbase/radius
    self.maxsteer=math.atan2(self._length,rIdeal)
    # the ideal max steering angle we can command is now set
    rospy.loginfo(rospy.get_caller_id() + " maximum ideal steering angle set to {0}.".format(self.maxsteer))
     
    # the format(ns) looks for the namespace in the ros parameter server, I guess
    
    self._left_steer_cmd_pub = rospy.Publisher('left_steering_ctrlr/command'.format(ns), Float64, queue_size=1)
    self._right_steer_cmd_pub = rospy.Publisher('right_steering_ctrlr/command'.format(ns), Float64, queue_size=1)
    self._left_back_axle_cmd_pub = rospy.Publisher('left_rear_velocity_ctrlr/command'.format(ns), Float64, queue_size=1)
    self._right_back_axle_cmd_pub = rospy.Publisher('right_rear_velocity_ctrlr/command'.format(ns), Float64, queue_size=1)
    self._left_front_axle_cmd_pub = rospy.Publisher('left_front_velocity_ctrlr/command'.format(ns), Float64, queue_size=1)
    self._right_front_axle_cmd_pub = rospy.Publisher('right_front_velocity_ctrlr/command'.format(ns), Float64, queue_size=1)
    self.ackermann_pub = rospy.Publisher('custom_ackerMsg',Float32MultiArray,queue_size=1)
    self.image_steering_topic = rospy.Subscriber("/steer_angle_img",Float64,self.callback_str_img)
    rospy.Subscriber('cmd_vel'.format(ns), Twist, self.cmd_vel_callback)

    

  def cmd_vel_callback(self, data):

      """Twist cmd_vel command callback

      :Parameters:
      cmd_vel : geometry_msgs/Twist Message
      vector3 linear (float64)(x,y,z)
      vector3 angular (float64)(x,y,z)
      2.8101
      """ 
      self._Phi_global[0][0] = data.linear.x 
      self._Phi_global[1][0] = 0
      self._Phi_global[2][0] = data.angular.z
      #self._Phi_global[0][0] = 0.5 
      #self._Phi_global[1][0] = 0
      #self._Phi_global[2][0] = 0
      

  def callback_str_img(self,angle):
      #print(type(data))
      self._steer_ang_img = angle.data*-1 #Angle taken from IP
      self.lastMsg = rospy.Time.now() 
    
  #Now adding rolling constring and slading constraing to the kinematics motion
  # Methode for for control the vehicle using Sliding constraint & Rolling constraint 
  def spin (self):

    if rospy.Time.now() - self.lastMsg > self.timeout:
        rospy.loginfo(rospy.get_caller_id() + " time out waiting for new input, setting velocity to 0.")
        self._Phi_global[0][0] = 0.0 
        self._Phi_global[1][0] = 0
        self._Phi_global[2][0] = 0.0
        return

    #First value of ICR distance for theorical Vlinear/Vangular
    #Motion for turn and move
    if(self._Phi_global[0][0]!=0 and self._steer_ang_img != 0):
      #print "++++++++++++++++++go straight and turn+++++++++++++++++"
      #self.ICR_distance[0] = (self._Phi_global[0][0]/self._Phi_global[2][0]) #Radio of ICR
      self.ICR_distance[0] = self._length/math.tan(self._steer_ang_img)
      #self.steer_ang = math.tan(self._length/self.ICR_distance[0]) #Angle of virtual wheel in center of the vehicle
      #print "self.ICR_distance[0], _steer_ang_img",self.ICR_distance[0],self._steer_ang_img      
    #If the value of Z dot is zero go straight
    if(self._steer_ang_img==0):
      self.ICR_distance[0] = 0;
      #self.steer_ang = 0; #Virtual wheel in center of the vehicle
      #print "++++++++++++++++++Zero turning+++++++++++++++++"
        
    #For online motion to left, means without linear velocity 
    if (self._Phi_global[0][0] == 0 and self._steer_ang_img > 0):
      #print "++++++++++++++++++turn left+++++++++++++++++"
      #self.ICR_distance[0] = 0.5;
      self.ICR_distance[0] = self._length/math.tan(self._steer_ang_img)
      #self.steer_ang = math.tan(self._length/self.ICR_distance[0]) #Virtual wheel in center of the vehicle
      #print "self.ICR_distance[0], _steer_ang_img",self.ICR_distance[0],self._steer_ang_img
      
    #For online motion to right, means without linear velocity 
    if (self._Phi_global[0][0] == 0 and self._steer_ang_img < 0):
      #print "++++++++++++++++++turn right+++++++++++++++++"
      #self.ICR_distance[0] = -0.5;
      self.ICR_distance[0] = self._length/math.tan(self._steer_ang_img)
      #Angle in radians of the steering virtual front  wheel
      #self.steer_ang = math.tan(self._length/self.ICR_distance[0]) #Virtual wheel in center of the vehicle
      #print "self.ICR_distance[0], _steer_ang_img",self.ICR_distance[0],self._steer_ang_img
      
    #Angle btw L's and Xg+ 
    # Count the wheels counter clockwise a - b - c - d
    alphaA = math.atan2(-self._width/2,self._length)
    alphaB = math.atan2(self._width/2,self._length)
    alphaC = math.pi/2 
    alphaD = -1*math.pi/2
    #print "rad", (self._steer_ang_img)
    #print "Ang" , self._steer_ang_img*180/math.pi


    #self.steer_ang = 0.5

    #Turn to left or right for theorical steer angle
    if(self._steer_ang_img > 0):
      #Angles reference to ICR for turn left
      SigmaAt = math.atan2(self._length,(self.ICR_distance[0]+(self._width/2)))
      SigmaBt = math.atan2(self._length,(self.ICR_distance[0]-(self._width/2)))
      #print "SigmaAt",SigmaAt
      #print "SigmaBt",SigmaBt
      #Angle btw wheel's axe and Global Y+   
      betaC = 0
      betaD = math.pi 
      self.betha_steer[1] = (math.pi/2 - SigmaBt + alphaB)*0.1
      self.betha_steer[0] = (math.pi/2 - SigmaAt + alphaA)*0.1
      #print "betha_steer",self.betha_steer
      #print "#Turn to left"
      #Turn to left
    if(self._steer_ang_img < 0):
      #Angles reference to ICR for turn right
      SigmaAt = math.atan2(self._length,(self.ICR_distance[0]-(self._width/2)))
      SigmaBt = math.atan2(self._length,(self.ICR_distance[0]+(self._width/2)))
      #print "SigmaAt",SigmaAt
      #print "SigmaBt",SigmaBt
      #Angle btw wheel's axe and Global Y+
      betaC = 0
      betaD = math.pi
      self.betha_steer[1] = (math.pi/2 - SigmaBt + alphaB )*0.1
      self.betha_steer[0] = (math.pi/2 - SigmaAt + alphaA)*0.1
      #print "betha_steer",self.betha_steer
      #print "#Turn to right"
    if(self._steer_ang_img == 0):
      #print "#go straight"
      #Angles reference to ICR for turn left
      SigmaAt = 0
      SigmaBt = 0
      #Angle btw wheel's axe and Global Y+
      betaC = 0
      betaD = math.pi
      self.betha_steer[1] = (math.pi/2 - SigmaBt + alphaB)*0.1
      self.betha_steer[0] = (math.pi/2 - SigmaAt + alphaA)*0.1
      #print "betha_steer",self.betha_steer
      #self.betha_steer[1] = (math.pi/2 - SigmaBt + alphaB) - self.offsetR
      #self.betha_steer[0] = (math.pi/2 - SigmaAt + alphaA) - self.offserL
      
      
    
    #Rolling constraint for theorical values
    J12 =  np.array([[math.sin(alphaA+self.betha_steer[0]),-1*math.cos(alphaA+self.betha_steer[0]),-1*self.distanceW2CG[0]*math.cos(self.betha_steer[0])],
    [math.sin(alphaB+self.betha_steer[1]),-1*math.cos(alphaB+self.betha_steer[1]),-1*self.distanceW2CG[1]*math.cos(self.betha_steer[1])],
    [math.sin(alphaC+betaC),-1*math.cos(alphaC+betaC),-1*self.distanceW2CG[2]*math.cos(betaC)],
    [math.sin(alphaD+betaD),-1*math.cos(alphaD+betaD),-1*self.distanceW2CG[3]*math.cos(betaD)]])
    #Sliding constraint
    C12 =  np.array([[math.cos(alphaA+self.betha_steer[0]),math.sin(alphaA+self.betha_steer[0]),self.distanceW2CG[0]*math.sin(self.betha_steer[0])],
    [math.cos(alphaB+self.betha_steer[1]),math.sin(alphaB+self.betha_steer[1]),self.distanceW2CG[1]*math.sin(self.betha_steer[1])],
    [math.cos(alphaC+betaC),math.sin(alphaC+betaC),self.distanceW2CG[2]*math.sin(betaC)],
    [math.cos(alphaD+betaD),math.sin(alphaD+betaD),self.distanceW2CG[3]*math.sin(betaD)]])
    J2 = np.eye(4,4)*self.radius # Both back wheels gives same constrains?
    
    A = np.concatenate([J12,C12],axis=0)
    #print "A matrix:", A
    zeros = np.zeros((4,4))
    B = np.concatenate([J2,zeros],axis=0)
    #print "Matrix B:", B
    B_trmp = np.transpose(B)
    #print "B transpose:", B_trmp
    B_inv = np.dot(B_trmp,B)
    #print "B inv 1:", B_inv
    B_inv = np.linalg.inv(B_inv)
    #print "B inv 2:", B_inv
    A_trmp = np.dot(B_trmp,A)
   #print "A dot:", A_trmp

    B_net = np.dot(B_inv,A_trmp)
    #print "B net:", B_net
    #print "Phi_global:", self._Phi_global
    # Translation speed of each wheel in local frame Consider back wheels moving as same speed
    Result = np.dot(B_net,self._Phi_global)
    #print "Result :", Result.shape
    #print "Result matrix:",Result
    self.wheel_ang_vel[0] = np.abs(Result[0]) #Wheel vel for A 
    self.wheel_ang_vel[1] = np.abs(Result[1]) #Wheel vel for B 
    self.wheel_ang_vel[2] = np.abs(Result[2])#Wheel vel for C
    self.wheel_ang_vel[3] = np.abs(Result[2]) #Wheel vel for D

        

    #print "Betha steer:",self.betha_steer
      
    #print "Angular Velocity:", self.wheel_ang_vel
    #print "-----------------"
    #Publish the steering and velocity commands for simulation.
    #Steering for PID controllers in simulation

    
    msgSteerL = Float64()
    msgSteerR = Float64()
    msgSteerL.data = self.betha_steer[1] 
    msgSteerR.data = self.betha_steer[0] 

    self._left_steer_cmd_pub.publish(msgSteerL)
    self._right_steer_cmd_pub.publish(msgSteerR) 
        
    #Rolling for PID controllers in simulation
    msgRollFL = Float64()
    msgRollFR = Float64()
    msgRollBL = Float64()
    msgRollBR = Float64()

    msgRollFL.data = self.wheel_ang_vel[1]
    msgRollFR.data = self.wheel_ang_vel[0]
    msgRollBL.data = self.wheel_ang_vel[2]
    msgRollBR.data = self.wheel_ang_vel[3]

    self._left_front_axle_cmd_pub.publish(msgRollFL)
    self._right_front_axle_cmd_pub.publish(msgRollFR)
    self._left_back_axle_cmd_pub.publish(msgRollBL)
    self._right_back_axle_cmd_pub.publish(msgRollBR)

    #Publish the steering and velocity commands for real HW.
    #Array contain steer_a,steer_b,vel_a,vel_b,vel_c,vel_d.
    self.array_HwCommands.data = [self.betha_steer[0],self.betha_steer[1],self.wheel_ang_vel[0],self.wheel_ang_vel[1],self.wheel_ang_vel[2],self.wheel_ang_vel[3]]
    rospy.loginfo(self.array_HwCommands)

    self.ackermann_pub.publish(self.array_HwCommands)

    #Sleep the node for the frecuency until next value
    

def main(argv):
  ns=''
  node = _AckermannCtrlr(ns)
  rate = rospy.Rate(50.0) 
  while not rospy.is_shutdown():
    node.spin()
    rate.sleep()

if __name__ == '__main__':
  main(sys.argv[1:])
  try: 
    listener('azcar_custom')
  except rospy.ROSInterruptException:
    pass


